public interface IList {
    void clear();
    int size();
    int get(int index);
    boolean add(int number);
    boolean add(int index, int number);
    int remove(int number);
    int removeByIndex(int index);
    boolean contains(int number);
    void print();
    int[] toArray();
    IList subList(int fromIndex, int toIndex);
    boolean removeAll(int[] arr);
    boolean retainAll(int[] arr);

}
