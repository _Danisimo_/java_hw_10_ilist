import org.junit.Assert;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Array;
import java.util.Arrays;

public class AListTest {

    private final int array[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    private final AList aList = new AList(array);

    private ByteArrayOutputStream output = new ByteArrayOutputStream();

    @Before
    public void setUp() {
        System.setOut(new PrintStream(output));
    }



    @Test
    public void sizeTest_Pass() {

        int size = aList.size();

        Assert.assertEquals(15, size);
    }

    @Test
    public void getTest_Pass() {

        int actualValue = aList.get(10);

        Assert.assertEquals(11, actualValue);
    }

}
