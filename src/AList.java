import java.util.Arrays;

public class AList  implements IList{

    int[] integer;
    int size;
    int index;

    public AList(){
        integer = new int[10];
    }

    public AList(int capacity){
        integer = new int[capacity + ((capacity / 100) * 20)];
    }

    public AList(int[] array){
        integer = new int[array.length + ((array.length / 100) * 20)];

        for (int i = 0; i < array.length; i++){
            integer[i] = array[i];
        }
    }


    @Override
    public void clear() {
        integer = new int[0];
    }

    @Override
    public int size() {
        return integer.length;
    }

    @Override
    public int get(int index) {
        return integer[index];
    }

    private void addArrayElements(){
    int[] newInts = new int[integer.length * 2];
        System.arraycopy(integer, 0, newInts, 0, index);
    integer = newInts;
}

    @Override
    public boolean add(int number) {
        if (index == integer.length) {
            addArrayElements();
        }
        integer[index] = number;
        index++;
        size++;
        return false;
    }

    @Override
    public boolean add(int index, int number) {
        if (index > integer.length || index < 0) {
            throw new ArrayIndexOutOfBoundsException("Index must be less than array length and under zero");
        }

        if (index > size - 1 && index < integer.length) {
            add(number);
        }
        int[] newInts = new int[(integer.length * 2)];

        newInts[index] = number;
        System.arraycopy(integer, index, newInts, index + 1,
                integer.length - index);
        integer = newInts;

        return false;
    }

    @Override
    public int remove(int number) {
        for (int i = 0; i < integer.length; i++) {
            if (integer[i] == number) {
                removeByIndex(i);
            }
        }
        return 0;
    }

    @Override
    public int removeByIndex(int index) {
        System.arraycopy(integer, index + 1, integer, index,
                integer.length - 1 - index);
        return 0;
    }

    @Override
    public boolean contains(int number) {
        for (int i = 0; i < size; i++) {
            if (integer[i] == number)
                return true;
        }

        return false;
    }

    @Override
    public void print() {
        System.out.println(Arrays.toString(toArray()));
        return ;
    }

    @Override
    public int[] toArray() {
        int[] resultInts = new int[index];
        if (index >= 0) System.arraycopy(integer, 0, resultInts, 0, index);
        return resultInts;
    }

    @Override
    public IList subList(int fromIndex, int toIndex) {
        int j = 0;
        int[] resultInts = new int[(toIndex - fromIndex) + 1];
        for (int i = fromIndex; i <= toIndex; i++) {
            resultInts[j++] = integer[i];
        }
        return new AList(resultInts);
    }

    @Override
    public boolean removeAll(int[] arr) {
        for (int j : arr) {
            remove(j);
        }
        return false;
    }

    @Override
    public boolean retainAll(int[] arr) {
        int[] resultInts = new int[integer.length];
        int j = 0;
        for (int anInt : integer) {
            for (int i : arr) {
                if (anInt == i) {
                    resultInts[j++] = anInt;
                }
            }
        }
        integer = resultInts;
        return false;
    }
}
