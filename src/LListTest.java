import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;


public class LListTest {

    private final int array[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    private final LList lList = new LList(array);


    private ByteArrayOutputStream output = new ByteArrayOutputStream();

    @Before
    public void setUp() throws Exception {
        System.setOut(new PrintStream(output));
    }



    @Test
    public void sizeTest_Pass() {

        int actual = lList.size();

        Assert.assertEquals(15, actual);
    }

    @Test
    public void getTest_Pass() {

        int actual = lList.get(5);

        Assert.assertEquals(6, actual);
    }

    @Test
    public void addTest_Pass() {
        boolean actual;

        actual = lList.add(21);

        Assert.assertTrue(actual);
    }

    @Test
    public void addIndexTest_Pass() {
        boolean actual;
        actual = lList.add(2, 21);

        Assert.assertTrue(actual);
    }


    @Test
    public void removeTest_Pass() {
        boolean actual;
        //actual = lList.remove();

       // Assert.assertTrue(actual);
    }

    @Test
    public void removeByIndex() {
        int actual;
       // actual = lList.remove();

      //  Assert.assertTrue(lList.removeByIndex(1));
    }

    @Test
    public void containsTest_Pass() {

        boolean actual;
        actual = lList.contains(10);

        Assert.assertTrue(actual);
    }

    @Test
    public void printTest_Pass() {

        lList.print();

        Assert.assertEquals("[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]", output.toString());
    }

    @Test
    public void toArrayTest_Pass() {

        int[] expected = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

        int[] actual = lList.toArray();

        Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void subListTest_Pass() {

        int[] expected = {6, 9, 10};

        int[] actual = (lList.subList(5, 7)).toArray();

        Assert.assertArrayEquals(expected, actual);

    }

    @Test
    public void removeAll() {

        boolean actual = lList.removeAll(array);

        Assert.assertTrue(actual);
    }

    @Test
    public void retainAll() {

        boolean actual = lList.retainAll(array);

        Assert.assertTrue(actual);
    }
}
